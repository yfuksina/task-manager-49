package ru.tsc.fuksina.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.fuksina.tm.api.service.ILoggerService;
import ru.tsc.fuksina.tm.dto.logger.EntityLogDTO;

import java.io.File;
import java.io.FileOutputStream;

public class LoggerService implements ILoggerService {

    @NotNull
    final public static String PROJECT_LOG_FILE_NAME = "./logger/project.log";

    @NotNull
    final public static String TASK_LOG_FILE_NAME = "./logger/task.log";

    @NotNull
    final public static String USER_LOG_FILE_NAME = "./logger/user.log";

    @Override
    @SneakyThrows
    public void writeLog(@NotNull EntityLogDTO message) {
        @Nullable final String className = message.getClassName();
        @Nullable final String fileName = getFileName(className);
        if (fileName == null) return;
        @NotNull final File file = new File(fileName);
        file.getParentFile().mkdirs();
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file, true);
        @NotNull final String header =
                "Id: " + message.getId() + "; Type: " + message.getType() + "; Date: " + message.getDate() + "\n";
        fileOutputStream.write(header.getBytes());
        fileOutputStream.write(message.getEntity().getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    private String getFileName(@NotNull final String className) {
        switch (className) {
            case "Project":
            case "ProjectDTO":
                return PROJECT_LOG_FILE_NAME;
            case "Task":
            case "TaskDTO":
                return TASK_LOG_FILE_NAME;
            case "User":
            case "UserDTO":
                return USER_LOG_FILE_NAME;
            default:
                return null;
        }
    }

}
