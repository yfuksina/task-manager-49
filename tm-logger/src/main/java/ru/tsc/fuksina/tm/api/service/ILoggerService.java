package ru.tsc.fuksina.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.fuksina.tm.dto.logger.EntityLogDTO;

public interface ILoggerService {

    void writeLog(@NotNull EntityLogDTO message);

}
