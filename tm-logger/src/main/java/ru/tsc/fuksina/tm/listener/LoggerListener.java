package ru.tsc.fuksina.tm.listener;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.fuksina.tm.api.service.ILoggerService;
import ru.tsc.fuksina.tm.dto.logger.EntityLogDTO;
import ru.tsc.fuksina.tm.service.LoggerService;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import java.io.Serializable;

public class LoggerListener implements MessageListener {

    @NotNull
    final ILoggerService loggerService = new LoggerService();

    @Override
    @SneakyThrows
    public void onMessage(@NotNull final Message message) {
        if (!(message instanceof ObjectMessage)) return;
        @NotNull final Serializable entity = ((ObjectMessage) message).getObject();
        if (entity instanceof EntityLogDTO) loggerService.writeLog((EntityLogDTO) entity);
    }

}
