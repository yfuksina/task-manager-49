package ru.tsc.fuksina.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.fuksina.tm.dto.logger.EntityLogDTO;

public interface ISenderService {

    void send(@NotNull EntityLogDTO log);

    @NotNull
    EntityLogDTO createMessage(@NotNull Object object, @NotNull String type);

}
