package ru.tsc.fuksina.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.tsc.fuksina.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

}
