package ru.tsc.fuksina.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.fuksina.tm.api.repository.dto.IProjectRepositoryDTO;
import ru.tsc.fuksina.tm.dto.model.ProjectDTO;
import ru.tsc.fuksina.tm.enumerated.Sort;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

public class ProjectRepositoryDTO extends AbstractUserOwnedRepositoryDTO<ProjectDTO> implements IProjectRepositoryDTO {

    public ProjectRepositoryDTO(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAll() {
        return entityManager.createQuery("SELECT m  FROM ProjectDTO m", ProjectDTO.class).getResultList();
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAll(@NotNull final String userId) {
        if (userId.isEmpty()) return Collections.emptyList();
        return entityManager.createQuery("SELECT m FROM ProjectDTO m WHERE m.userId = :userId", ProjectDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAll(@NotNull final String userId, @Nullable final Sort sort) {
        if (userId.isEmpty()) return Collections.emptyList();
        @NotNull String jpql = "SELECT m FROM ProjectDTO m WHERE m.userId = :userId ORDER BY m."
                + getSortType(sort.getComparator());
        return entityManager.createQuery(jpql, ProjectDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public ProjectDTO findOneById(@NotNull final String id) {
        return entityManager.find(ProjectDTO.class, id);
    }

    @Nullable
    @Override
    public ProjectDTO findOneById(@NotNull final String userId, @NotNull final String id) {
        if (userId.isEmpty() || id.isEmpty()) return null;
        return entityManager
                .createQuery("SELECT m FROM ProjectDTO m WHERE m.userId = :userId AND m.id = :id", ProjectDTO.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void removeById(@NotNull final String id) {
        entityManager.remove(entityManager.getReference(ProjectDTO.class, id));
    }

    @Override
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        entityManager.remove(findOneById(userId, id));
    }

    @Override
    public void clear() {
        entityManager.createQuery("DELETE FROM ProjectDTO").executeUpdate();
    }

    @Override
    public void clear(@NotNull final String userId) {
        entityManager.createQuery("DELETE FROM ProjectDTO m WHERE m.userId = :userId")
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        return entityManager.createQuery("SELECT COUNT(m) = 1 FROM ProjectDTO m WHERE m.id = :id", Boolean.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        return entityManager
                .createQuery("SELECT COUNT(m) = 1 FROM ProjectDTO m WHERE m.id = :id AND m.userId = :userId", Boolean.class)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .getSingleResult();
    }

    @Override
    public long getSize() {
        return entityManager.createQuery("SELECT COUNT(m) FROM ProjectDTO m", Long.class)
                .getSingleResult();
    }

    @Override
    public long getSize(@NotNull String userId) {
        return entityManager.createQuery("SELECT COUNT(m) FROM ProjectDTO m WHERE m.userId = :userId", Long.class)
                .setParameter("userId", userId)
                .getSingleResult();
    }

}
