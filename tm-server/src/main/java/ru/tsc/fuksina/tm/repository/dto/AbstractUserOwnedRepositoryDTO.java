package ru.tsc.fuksina.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.tsc.fuksina.tm.api.repository.dto.IUserOwnedRepositoryDTO;
import ru.tsc.fuksina.tm.comparator.CreatedComparator;
import ru.tsc.fuksina.tm.comparator.DateStartComparator;
import ru.tsc.fuksina.tm.comparator.StatusComparator;
import ru.tsc.fuksina.tm.dto.model.AbstractUserOwnedModelDTO;

import javax.persistence.EntityManager;
import java.util.Comparator;

public abstract class AbstractUserOwnedRepositoryDTO<M extends AbstractUserOwnedModelDTO> extends AbstractRepositoryDTO<M> implements IUserOwnedRepositoryDTO<M> {

    public AbstractUserOwnedRepositoryDTO(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void add(@NotNull final String userId, final @NotNull M model) {
        if (userId.isEmpty()) return;
        entityManager.persist(model);
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final M model) {
        if (userId.isEmpty()) return;
        entityManager.remove(model);
    }

    public void update(@NotNull final String userId, @NotNull final M model) {
        if (userId.isEmpty()) return;
        entityManager.merge(model);
    }

    @NotNull
    protected String getSortType(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return "created";
        else if (comparator == StatusComparator.INSTANCE) return "status";
        else if (comparator == DateStartComparator.INSTANCE) return "status";
        else return "name";
    }

}
