package ru.tsc.fuksina.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.fuksina.tm.api.repository.dto.IUserOwnedRepositoryDTO;
import ru.tsc.fuksina.tm.dto.model.AbstractUserOwnedModelDTO;

import java.util.Date;

public interface IUserOwnedServiceDTO<M extends AbstractUserOwnedModelDTO> extends IUserOwnedRepositoryDTO<M>, IServiceDTO<M> {

    @NotNull
    M create(@Nullable String userId, @Nullable String name);

    @NotNull
    M create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    M create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description,
            @Nullable Date dateStart,
            @Nullable Date dateEnd
    );

    @NotNull
    M updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

}
